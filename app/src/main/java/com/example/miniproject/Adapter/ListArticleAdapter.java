package com.example.miniproject.Adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.miniproject.R;
import com.example.miniproject.entities.Article;

import java.util.List;

public class ListArticleAdapter extends RecyclerView.Adapter<ListArticleAdapter.ViewHolder> {
    List<Article> articles;
    Context context;
    Article article;
    OnClickRecyclerView onClickRecyclerView;

    public ListArticleAdapter(List<Article> articles, Context context, OnClickRecyclerView onClickRecyclerView) {
        this.articles = articles;
        this.context = context;
        this.onClickRecyclerView = onClickRecyclerView;

    }

    public ListArticleAdapter(List<Article> articles, Context context) {
        this.articles = articles;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listArticle = layoutInflater.inflate(R.layout.article_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(listArticle);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        article = articles.get(position);
        holder.bookTilteText.setText(article.getTitle());
        holder.bookDescTextView.setText(article.getDescription());
        holder.bookCreatedDate.setText(article.getCreatedDate());
        final String image = article.getImage();
        if (image == null || image.equals(null) || image.equals("string")) {
            holder.imageView.setImageResource(R.drawable.default_image);
        } else {

            Uri imageUri = Uri.parse(image);
            Glide.with(holder.imageView.getContext()).load(imageUri).into(holder.imageView);
        }


    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, option;
        TextView bookTilteText, bookCreatedDate, bookDescTextView;
        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.article_imageView);
            bookTilteText = itemView.findViewById(R.id.book_title_textview);
            bookCreatedDate = itemView.findViewById(R.id.created_date_textview);
            bookDescTextView = itemView.findViewById(R.id.book_desc_textview);
            option = itemView.findViewById(R.id.option);
            linearLayout = itemView.findViewById(R.id.article_layout_linear);

            imageView.setOnClickListener(v -> {
                Article arti = articles.get(getAdapterPosition());
                int id = arti.getId();
                Log.i("AdapterPosition", "ViewHolder: " + getAdapterPosition());
                onClickRecyclerView.onItemClickView(v, id, arti);
            });

            option.setOnClickListener(v -> {


                Article art = articles.get(getAdapterPosition());
                int id = art.getId();
                onClickRecyclerView.onItemClickOption(v, id, art);
            });
        }
    }
}
