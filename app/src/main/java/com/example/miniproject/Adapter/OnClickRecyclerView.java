package com.example.miniproject.Adapter;

import android.view.View;

import com.example.miniproject.entities.Article;

public interface OnClickRecyclerView {
    void onItemClickView(View view, int position, Article article);
    void onItemClickOption (View view, int id, Article article);
}
