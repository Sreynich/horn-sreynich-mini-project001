package com.example.miniproject.data.service;

import com.example.miniproject.entities.Article;
import com.example.miniproject.entities.ResponseArticles;
import com.example.miniproject.entities.ResponseImage;
import com.example.miniproject.entities.ResponseOneArticle;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ResponseArticles> getResponseArticle(@Query("page") int page, @Query("limit") int limit);

    @GET("v1/api/articles/{id}")
    Call<ResponseArticles> findArticleByID(@Path("id") int id);

    @POST("v1/api/articles")
    Call<Article> addArticle( @Body Article article);

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<ResponseImage> uploadImage(@Part MultipartBody.Part file);

    @DELETE("v1/api/articles/{id}")
    Call<ResponseOneArticle> deleteArticle(@Path("id") int id);

    @PUT("v1/api/articles/{id}")
    Call<ResponseOneArticle> updateArticle(@Path("id") int id, @Body Article article);
}
