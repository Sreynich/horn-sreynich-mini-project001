package com.example.miniproject.entities;

import com.google.gson.annotations.SerializedName;

public class ResponseOneArticle {
    @SerializedName("DATA")
    private Article data;

    @Override
    public String toString() {
        return "ResponseOneArticle{" +
                "data=" + data +
                '}';
    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public ResponseOneArticle(Article data) {
        this.data = data;
    }
}
