package com.example.miniproject.ui.home.mvp;


import com.example.miniproject.data.ServiceGenerator;
import com.example.miniproject.data.service.ArticleService;
import com.example.miniproject.entities.Article;
import com.example.miniproject.entities.ResponseArticles;
import com.example.miniproject.entities.ResponseOneArticle;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleInteracter implements HomeMVP.Interacter {
    ArticleService articleService;

    public ArticleInteracter() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    @Override
    public void getArticle(int page, int limit, final OnGotArticleInteracted onGotArticleInteracted) {


        Call<ResponseArticles> articleCall = articleService.getResponseArticle(page, limit);
        articleCall.enqueue(new Callback<ResponseArticles>() {
            @Override
            public void onResponse(Call<ResponseArticles> call, Response<ResponseArticles> response) {
                List<Article> articles = response.body().getData();
                onGotArticleInteracted.onSuccess(articles);
            }

            @Override
            public void onFailure(Call<ResponseArticles> call, Throwable t) {
                onGotArticleInteracted.onFail(t.toString());
            }
        });
    }

    private static final String TAG = "ArticleInteracter";

    @Override
    public void deleteArticle(int id, OnDeleteArticleInteracter onDeleteArticleInteracter) {
        Call<ResponseOneArticle> articleCall = articleService.deleteArticle(id);
        articleCall.enqueue(new Callback<ResponseOneArticle>() {
            @Override
            public void onResponse(Call<ResponseOneArticle> call, Response<ResponseOneArticle> response) {
                onDeleteArticleInteracter.onSuccess(" Delete Successfully");
            }

            @Override
            public void onFailure(Call<ResponseOneArticle> call, Throwable t) {

                onDeleteArticleInteracter.onFail(t.getMessage());
            }
        });
    }
}
