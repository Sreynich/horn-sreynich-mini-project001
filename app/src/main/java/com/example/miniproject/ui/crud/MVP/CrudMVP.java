package com.example.miniproject.ui.crud.MVP;

import android.content.Context;
import android.net.Uri;

import com.example.miniproject.entities.Article;

public interface CrudMVP {

    interface View {

        void onAddArticleSuccess(String message);

        void onAddArticleFail(String message);


        void onUploadImageSuccess(String url);

        void onUploadImageFail(String message);

        void onUpdateSuccess(String message);

        void onUpdateFail(String message);

    }

    interface Presenter {
        void addArticle(Article article);

        void upLoadImage(Context context, Uri uri);

        void updateArticle(int id, Article article);

        void onSetView(View view);

    }

    interface Interacter {
        void addArticle(Article article, OnAddArticleInterater onArticleInterater);

        void uploadImage(Context context, Uri uri, OnUploadImageInteracter onUploadImageInteracter);

        void updateArticle(int id, Article article, OnUpdateArticleInteracter onUpdateArticleInteracter);

        interface OnAddArticleInterater {
            void onSuccessed(String message);

            void onFailed(String message);
        }


        interface OnUploadImageInteracter {
            void onSuccess(String url);

            void onFail(String message);
        }

        interface OnUpdateArticleInteracter {
            void onSuccessed(String message);

            void onFailed(String message);
        }
    }
}
