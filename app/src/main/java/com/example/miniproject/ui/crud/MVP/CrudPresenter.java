package com.example.miniproject.ui.crud.MVP;

import android.content.Context;
import android.net.Uri;

import com.example.miniproject.entities.Article;

public class CrudPresenter implements CrudMVP.Presenter {
    private CrudMVP.Interacter interacter;
    private CrudMVP.View view;

    public CrudPresenter() {
        interacter = new CrudInteracter();
    }

    @Override
    public void addArticle(Article article) {
        interacter.addArticle(article, new CrudMVP.Interacter.OnAddArticleInterater() {
            @Override
            public void onSuccessed(String message) {
                view.onAddArticleSuccess(message);
            }

            @Override
            public void onFailed(String message) {
                view.onAddArticleFail(message);
            }
        });
    }


    @Override
    public void upLoadImage(Context context, Uri uri) {
        interacter.uploadImage(context, uri, new CrudMVP.Interacter.OnUploadImageInteracter() {
            @Override
            public void onSuccess(String url) {
                view.onUploadImageSuccess(url);
            }

            @Override
            public void onFail(String message) {
                view.onUploadImageFail(message);
            }
        });
    }

    @Override
    public void updateArticle(int id, Article article) {
        interacter.updateArticle(id, article, new CrudMVP.Interacter.OnUpdateArticleInteracter() {
            @Override
            public void onSuccessed(String message) {
                view.onUpdateSuccess(message);
            }

            @Override
            public void onFailed(String message) {
                view.onUpdateFail(message);
            }
        });
    }

    @Override
    public void onSetView(CrudMVP.View view) {
        this.view = view;
    }
}
