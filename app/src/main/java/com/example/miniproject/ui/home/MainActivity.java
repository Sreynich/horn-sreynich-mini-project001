package com.example.miniproject.ui.home;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.miniproject.Adapter.ListArticleAdapter;
import com.example.miniproject.Adapter.OnClickRecyclerView;
import com.example.miniproject.R;
import com.example.miniproject.entities.Article;
import com.example.miniproject.ui.Detial.DetailArticleActivity;
import com.example.miniproject.ui.crud.CrudActivity;
import com.example.miniproject.ui.home.mvp.ArticlePresenter;
import com.example.miniproject.ui.home.mvp.HomeMVP;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.ArrayList;
import java.util.List;

import retrofit2.http.POST;

public class MainActivity extends AppCompatActivity implements HomeMVP.View, OnClickRecyclerView {

    RecyclerView recyclerView;
    List<Article> listArticles;
    ListArticleAdapter listArticleAdapter;
    HomeMVP.Presenter presenter;
    SwipeRefreshLayout swipeRefreshLayout;
    int currentPage = 1;
    final int LIMIT = 15;
    ImageView option;

    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 4;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    ProgressBar progressBar;
    FloatingActionButton buttonAdd;
    Intent intent;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new ArticlePresenter();
        presenter.setView(this);
        onViewInitialized();
        recyclerviewListener();
        swipeRefreshLayout.setOnRefreshListener(() -> {
           refreshData();
        });

        buttonAdd.setOnClickListener(v -> {
            intent = new Intent(MainActivity.this, CrudActivity.class);
            intent.putExtra("BUTTON", 1);
            startActivity(intent);
        });


    }

    private void onViewInitialized() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.recycler_view);
        swipeRefreshLayout = findViewById(R.id.swipe);
        progressBar = findViewById(R.id.progress_bar);
        listArticles = new ArrayList<>();
        listArticleAdapter = new ListArticleAdapter(listArticles, getApplicationContext(), this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(listArticleAdapter);
        buttonAdd = findViewById(R.id.floating_button);
        initializeData();
    }

    private void initializeData() {
        currentPage = 1;
        presenter.onRequestNextPage(currentPage, LIMIT);


    }


    @Override
    public void onGetArticleSuccess(List<Article> articles) {

        listArticles.addAll(articles);
        listArticleAdapter.notifyDataSetChanged();
        currentPage++;

    }

    @Override
    public void onDeleteArticleSecceess(String message) {

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        refreshData();

    }

    private void refreshData(){
        listArticles.clear();
        listArticleAdapter.notifyDataSetChanged();
        initializeData();
        previousTotal = 0;
        visibleThreshold = 4;
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDeleteArticleFail(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGetArticleFail(String message) {
        Log.d("onSuccess", "onGetArticleSuccess: " + "ERROR" + message);


    }


    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    private void recyclerviewListener() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                // Handling the infinite scroll
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    presenter.onRequestNextPage(currentPage, LIMIT);
                    loading = true;
                }

            }
        });


    }

    @Override
    public void onItemClickView(View view, int position, Article article) {
        Intent intent = new Intent(getApplicationContext(), DetailArticleActivity.class);
        intent.putExtra("TITLE", article.getTitle());
        intent.putExtra("DESC", article.getDescription());
        intent.putExtra("DATE", article.getCreatedDate());
        intent.putExtra("IMAGE", article.getImage());
        startActivity(intent);
    }


    @Override
    public void onItemClickOption(View view, int id, Article art) {
        option = view.findViewById(R.id.option);
        PopupMenu popupMenu = new PopupMenu(getApplicationContext(), option);
        popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case (R.id.option_delete):
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Warning !");
                    builder.setMessage("Do you want to delete it?");
                    builder.setPositiveButton("Yes", (dialog, which) -> {

                        presenter.deleteArticle(id);

                    });
                    builder.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
                    builder.show();
                    return true;
                case (R.id.option_update):
                    intent = new Intent(MainActivity.this, CrudActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("BUTTON", 2);
                    intent.putExtra("TITLE", art.getTitle());
                    intent.putExtra("DESC", art.getDescription());
                    intent.putExtra("DATE", art.getCreatedDate());
                    intent.putExtra("IMAGE", art.getImage());
                    startActivity(intent);

            }


            return false;
        });
        popupMenu.show();
    }
}