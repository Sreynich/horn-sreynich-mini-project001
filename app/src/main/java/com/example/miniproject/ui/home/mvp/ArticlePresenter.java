package com.example.miniproject.ui.home.mvp;

import com.example.miniproject.entities.Article;

import java.util.List;

public class ArticlePresenter implements HomeMVP.Presenter {

    private HomeMVP.View view;
    private HomeMVP.Interacter interacter;

    public ArticlePresenter() {
        interacter = new ArticleInteracter();
    }


    @Override
    public void onRequestNextPage(int page, int limit) {
        view.showLoading();
        interacter.getArticle(page, limit, new HomeMVP.Interacter.OnGotArticleInteracted() {
            @Override
            public void onSuccess(List<Article> articles) {
                view.onGetArticleSuccess(articles);
                view.hideLoading();
            }

            @Override
            public void onFail(String message) {
                view.onGetArticleFail(message);
                view.hideLoading();
            }
        });
    }

    @Override
    public void deleteArticle(int id) {
        interacter.deleteArticle(id, new HomeMVP.Interacter.OnDeleteArticleInteracter() {
            @Override
            public void onSuccess(String message) {
                view.onDeleteArticleSecceess(message);
            }

            @Override
            public void onFail(String message) {
                view.onDeleteArticleFail(message);
            }
        });
    }

    @Override
    public void setView(HomeMVP.View view) {
        this.view = view;

    }
}
