package com.example.miniproject.ui.crud;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.miniproject.R;
import com.example.miniproject.entities.Article;
import com.example.miniproject.ui.crud.MVP.CrudMVP;
import com.example.miniproject.ui.crud.MVP.CrudPresenter;
import com.example.miniproject.ui.home.MainActivity;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class CrudActivity extends AppCompatActivity implements CrudMVP.View, EasyPermissions.PermissionCallbacks {

    ImageView imageView;
    EditText editTitle, editDesc;
    Button buttonOk, buttonCancel;
    CrudMVP.Presenter presenter;
    Intent intent;
    String imageUriString;
    Uri imageUri;
    Article article = new Article();
    String image;
    String desc;
    String title;
    Article newArtilce = new Article();


    public CrudActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud);
        onViewInitialized();
        intent = getIntent();
        int button = intent.getIntExtra("BUTTON", 0);
        if (button == 1) {
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addArticle();

                }
            });
        }
        if (button == 2) {
            setOldData();
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = intent.getIntExtra("ID", 0);
                    updateArticle(id);
                }
            });
        }
        buttonCancel.setOnClickListener(v -> finish());
        imageView.setOnClickListener(v -> uploadImage());

    }

    void setOldData() {
        title = intent.getStringExtra("TITLE");
        desc = intent.getStringExtra("DESC");
        image = intent.getStringExtra("IMAGE");

        editTitle.setText(title);
        editDesc.setText(desc);

        if (image == null || image.equals(null) || image.equals("string")) {
            imageView.setImageResource(R.drawable.default_image);
        } else {

            Uri imageUri = Uri.parse(image);
            Glide.with(imageView.getContext()).load(imageUri).into(imageView);
        }

    }

    private void onViewInitialized() {
        imageView = findViewById(R.id.image_view);
        editTitle = findViewById(R.id.edit_text_title);
        editDesc = findViewById(R.id.edit_text_desc);
        buttonOk = findViewById(R.id.button_ok);
        buttonCancel = findViewById(R.id.button_cancel);

        presenter = new CrudPresenter();
        presenter.onSetView(this);
    }

    @Override
    public void onAddArticleSuccess(String message) {
        Toast.makeText(getApplicationContext(), "Add article successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAddArticleFail(String message) {
        Toast.makeText(getApplicationContext(), "Fail aii" + message, Toast.LENGTH_LONG).show();

    }


    @Override
    public void onUploadImageSuccess(String u) {
        article.setImage(u);
        newArtilce.setImage(u);
    }

    @Override
    public void onUploadImageFail(String message) {
        Log.i("URL", " upload fail aii");
    }

    @Override
    public void onUpdateSuccess(String message) {
        Toast.makeText(getApplicationContext(), message , Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUpdateFail(String message) {
        Toast.makeText(getApplicationContext(), message , Toast.LENGTH_LONG).show();
    }

    private void addArticle() {

        article.setTitle(editTitle.getText().toString());
        article.setDescription(editDesc.getText().toString());
        presenter.addArticle(article);

        Intent intent = new Intent(CrudActivity.this, MainActivity.class);
        finish();
        startActivity(intent);

    }

    @AfterPermissionGranted(123)
    void uploadImage() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(getApplicationContext(), permissions)) {
            chooseImage();
        } else {
            EasyPermissions.requestPermissions(this, "We need the permission", 123, permissions);
        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1000);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        chooseImage();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Toast.makeText(getApplicationContext(), "permission denied", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            imageUri = data.getData();
            imageUriString = imageUri.toString();
            try {
                InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);

                presenter.upLoadImage(getApplicationContext(), imageUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }

    void updateArticle(int id) {

        newArtilce.setTitle(editTitle.getText().toString());
        newArtilce.setDescription(editDesc.getText().toString());
        if (newArtilce.getImage() == null) {
            newArtilce.setImage(image);
        }
        presenter.updateArticle(id, newArtilce);
        Intent intent = new Intent(CrudActivity.this, MainActivity.class);
        finish();
        startActivity(intent);
    }
}
