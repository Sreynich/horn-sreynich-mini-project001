package com.example.miniproject.ui.Detial;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.miniproject.R;

public class DetailArticleActivity extends AppCompatActivity {
    TextView textTitle, textDesc, textDate;
    ImageView imageView;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);

        textTitle = findViewById(R.id.article_title);
        textDesc = findViewById(R.id.article_desc);
        textDate = findViewById(R.id.article_date);
        imageView = findViewById(R.id.article_image);
        button = findViewById(R.id.article_button_ok);
        button.setOnClickListener(v -> finish());


        Intent intent = getIntent();
        String title = intent.getStringExtra("TITLE");
        String desc = intent.getStringExtra("DESC");
        String date = intent.getStringExtra("DATE");
        String image = intent.getStringExtra("IMAGE");

        textTitle.setText(title);
        textDesc.setText("Description:  " + desc);
        textDate.setText("Date:  " + date);

        if (image == null || image.equals(null) || image.equals("string")) {
            imageView.setImageResource(R.drawable.default_image);
        } else {

            Uri imageUri = Uri.parse(image);
            Glide.with(imageView.getContext()).load(imageUri).into(imageView);
        }

    }

}
