package com.example.miniproject.ui.crud.MVP;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.example.miniproject.data.ServiceGenerator;
import com.example.miniproject.data.service.ArticleService;
import com.example.miniproject.entities.Article;
import com.example.miniproject.entities.ResponseImage;
import com.example.miniproject.entities.ResponseOneArticle;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrudInteracter implements CrudMVP.Interacter {

    ArticleService articleService;

    public CrudInteracter() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    @Override
    public void addArticle(Article article, OnAddArticleInterater onAddArticleInterater) {


        Call call = articleService.addArticle(article);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                onAddArticleInterater.onSuccessed("Success");
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                onAddArticleInterater.onFailed(t.toString());
            }
        });
    }

    @Override
    public void uploadImage(Context context, Uri uri, OnUploadImageInteracter onUploadImageInteracter) {
        String realPath = getRealPathFromUri(context, uri);

        File file = new File(realPath);
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("FILE", file.getName(), fileReqBody);

        Call<ResponseImage> articleCall = articleService.uploadImage(part);
        articleCall.enqueue(new Callback<ResponseImage>() {
            @Override
            public void onResponse(Call<ResponseImage> call, Response<ResponseImage> response) {

                String url = response.body().getDATA();

                onUploadImageInteracter.onSuccess(url);
            }

            @Override
            public void onFailure(Call<ResponseImage> call, Throwable t) {
                onUploadImageInteracter.onFail(t.toString());
            }
        });
    }


    @Override
    public void updateArticle(int id, Article article, OnUpdateArticleInteracter onUpdateArticleInteracter) {


        Call<ResponseOneArticle> articleCall = articleService.updateArticle(id, article);
        articleCall.enqueue(new Callback<ResponseOneArticle>() {
            @Override
            public void onResponse(Call<ResponseOneArticle> call, Response<ResponseOneArticle> response) {
                onUpdateArticleInteracter.onSuccessed("Update Successfully");
            }

            @Override
            public void onFailure(Call<ResponseOneArticle> call, Throwable t) {
                onUpdateArticleInteracter.onFailed(t.getMessage());
            }
        });

    }

    public String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
