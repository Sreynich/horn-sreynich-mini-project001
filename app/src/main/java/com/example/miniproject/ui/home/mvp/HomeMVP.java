package com.example.miniproject.ui.home.mvp;

import com.example.miniproject.entities.Article;

import java.util.List;

public interface HomeMVP {
    interface View {

        void onGetArticleFail(String message);

        void onGetArticleSuccess(List<Article> articles);

        void onDeleteArticleSecceess(String message);

        void onDeleteArticleFail(String message);

        void showLoading();

        void hideLoading();

    }

    interface Presenter {
        void onRequestNextPage(int page, int limit);

        void deleteArticle(int id);

        void setView(View view);
    }

    interface Interacter {

        void getArticle(int page, int limit, OnGotArticleInteracted onGotArticleInteracted);

        void deleteArticle(int id, OnDeleteArticleInteracter onDeleteArticleInteracter);

        interface OnGotArticleInteracted {
            void onSuccess(List<Article> articles);

            void onFail(String message);
        }

        interface OnDeleteArticleInteracter {
            void onSuccess(String message);

            void onFail(String message);
        }
    }
}
